<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Hash;

class AuthController extends Controller
{
    public function register(Request $request)
      {
        $api_key = $request->header('x-api-key');
        if($api_key != null){
            if($api_key == env('API_KEY')){
                //validation
                $validator = Validator::make($request->all(),[
                    'firstname'   => 'required|string|max:255',
                    'middlename'  => 'nullable|string|max:255',
                    'lastname'    => 'required|string|max:255',
                    'birthday'    => 'required|date',
                    'subdivision' => 'nullable|string|max:255',
                    'street'      => 'nullable|string|max:255',
                    'city'        => 'nullable|string|max:255',
                    'province'    => 'nullable|string|max:255',
                    'country'     => 'nullable|string|max:255',
                    'zipcode'     => 'nullable|integer|min:4',
                    'email'       => 'required|email|unique:users',
                    'username'    => 'required|string|max:255|unique:users',
                    'password'    => 'required|confirmed|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%@#^&*:;?]).*$/',
                ]);


                if($validator->fails())
                {
                    $code  = 400;
                    return response()->json([
                        'status'=>400,
                        'errors'=>$validator->messages(),
                    ], $code);
                }
                else{
                    $data = $request->all();
                    $data['password'] = Hash::make($data['password']);
                    $user = User::create($data);

                    // result with API  token in response
                    $code  = 200;
                    return response()->json([
                        'status'  => 'Success',
                        'message' => "Registrations success",
                        ], $code);
                }
            }
            else{
                $code  = 401;
                return response()->json([
                    'status'=>401,
                    'message'=>"Unauthorized.",
                ], $code);
            }
        }else{
            $code  = 401;
                return response()->json([
                    'status'=>401,
                    'message'=>"Unauthorized.",
                ], $code);
        }
      }

      public function login(Request $request)
      {
        $api_key = $request->header('x-api-key');

        if($api_key != null){
            if($api_key == env('API_KEY')){
                 //validate data input
            $validator = Validator::make($request->all(),[
                'email' => 'email|required',
                'password' => 'required|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%@#^&*:;?]).*$/'
            ]);
  
            $userData = array(
                  'email' => $request->get('email'),
                  'password' => $request->get('password')
            );
            //if fails return 400 error
            if($validator->fails())
              {
                  $code  = 400;
                  return response()->json([
                      'status'=>400,
                      'errors'=>$validator->messages(),
                  ], $code);
              }
           else {
                   //if credentials does not match return 401 error
                  if (!Auth::attempt($userData)) {
                      $code  = 401;
                      return response()->json([
                          'status'=>401,
                          'message'=>"Credentials not match.",
                      ], $code);
                  } 
                  else{
                      //success login
                      $token = auth()->user()->createToken('APItoken')->plainTextToken;
                      $code  = 200;
                      return response()->json([
                      'status'  => 'Success',
                      'message' => "Login success",
                      'token'   => $token,
                      ], $code);
                  }
            }
            }
            else{
                $code  = 401;
                return response()->json([
                    'status'=>401,
                    'message'=>"Unauthorized.",
                ], $code);
            }
        }else{
            $code  = 401;
                return response()->json([
                    'status'=>401,
                    'message'=>"Unauthorized.",
                ], $code);
        }
      }

      public function logout(Request $request){

        $api_key = $request->header('x-api-key');

        if($api_key != null){
            if($api_key == env('API_KEY')){
                auth()->user()->tokens()->delete();

                $code = 200;
                return response()->json([
                    'status'  => 'Logout success',
                    'message' => 'Token revoked',
                ], $code);
            }
            else{
                $code  = 401;
                return response()->json([
                    'status'=>401,
                    'message'=>"Unauthorized.",
                ], $code);
            }
        }else{
            $code  = 401;
                return response()->json([
                    'status'=>401,
                    'message'=>"Unauthorized.",
                ], $code);
        }
      }


}

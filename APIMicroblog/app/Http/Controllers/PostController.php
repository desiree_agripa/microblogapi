<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Comments;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $api_key = $request->header('x-api-key');
        if($api_key != null){
            if($api_key == env('API_KEY')){

                $posts = Post::whereIn('user_id', auth()->user())->with('comments')->get();
                $code = 200;
                return response()->json([
                    'results' => $posts,
                ], $code);

            }else{
                $code  = 401;
                return response()->json([
                    'status'=>401,
                    'message'=>"Unauthorized.",
                ], $code);
            }
        }else{
            $code  = 401;
                return response()->json([
                    'status'=>401,
                    'message'=>"Unauthorized.",
                ], $code);
        }
        
    }

    public function posts(Request $request,$id)
    {
        $api_key = $request->header('x-api-key');

        if($api_key != null){
            if($api_key == env('API_KEY')){

                $post = Post::where('id', $id)->with('comments')->get();
                $code = 200;
                return response()->json([
                    'results' => $post,
                ], $code);
            }
            else{
                $code  = 401;
                return response()->json([
                    'status'=>401,
                    'message'=>"Unauthorized.",
                ], $code);
            }
        }else{
            $code  = 401;
                return response()->json([
                    'status'=>401,
                    'message'=>"Unauthorized.",
                ], $code);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
